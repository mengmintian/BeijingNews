package net.mtian.beijingnews.pager;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

import net.mtian.beijingnews.base.BasePager;

/**
 *智慧服务
 * Created by mengm on 2017/1/2.
 */

public class SmartServicePager extends BasePager {
    public SmartServicePager(Context context) {
        super(context);
    }

    @Override
    public void initData() {
        super.initData();
        //1、设置标题
        tv_title.setText("智慧服务");
        //2、联网请求得到数据，创建视图
        TextView textView = new TextView(content);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.RED);
        textView.setTextSize(25);
        //3、把子视图添加到BasePager的FrameLayout中
        fl_content.addView(textView);
        //4、绑定数据
        textView.setText("智慧服务页面内容");
    }
}
