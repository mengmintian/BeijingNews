package net.mtian.beijingnews.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by mengm on 2017/1/1.
 */

public abstract class BaseFragment extends Fragment{
    
    public Activity context; //MainActivity

    /**
     * 当Fragment被创建的时候回调
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    /**
     * 当视图被创建的时候回调，创建了视图
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return initView();
    }

    /**
     * 让子类实现自己的视图，达到自己特有的效果
     * @return
     */
    public abstract View initView();

    /**
     * 当Activity被创建之后回调
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    /**
     * 1、如果子页面没有数据，联网请求数据，并绑定到initView创建的视图上
     * 2、有数据，直接绑定到initView创建的视图上
     */
    public void initData(){

    }
}
