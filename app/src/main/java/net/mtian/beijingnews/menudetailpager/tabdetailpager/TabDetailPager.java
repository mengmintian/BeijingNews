package net.mtian.beijingnews.menudetailpager.tabdetailpager;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import net.mtian.beijingnews.base.MenuDetailBasePager;
import net.mtian.beijingnews.domain.NewsCenterPagerBean2;

/**
 * Created by mengm on 2017/1/4.
 */

public class TabDetailPager extends MenuDetailBasePager{
    private final NewsCenterPagerBean2.DetailPagerData.ChildrenData childrenData;
    private TextView textView;

    public TabDetailPager(Context context, NewsCenterPagerBean2.DetailPagerData.ChildrenData childrenData) {
        super(context);

        this.childrenData = childrenData;
    }

    @Override
    public View initView() {
        textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.RED);
        textView.setTextSize(25);

        return textView;
    }

    @Override
    public void initData() {
        super.initData();
        textView.setText(childrenData.getTitle());
    }
}
