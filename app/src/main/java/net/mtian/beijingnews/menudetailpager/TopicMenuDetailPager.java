package net.mtian.beijingnews.menudetailpager;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import net.mtian.beijingnews.base.MenuDetailBasePager;

/**
 * Created by mengm on 2017/1/3.
 */

public class TopicMenuDetailPager extends MenuDetailBasePager{

    TextView textView;

    public TopicMenuDetailPager(Context context){
        super(context);

    }

    @Override
    public View initView() {
        textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.RED);
        textView.setTextSize(25);

        return textView;
    }

    @Override
    public void initData() {
        super.initData();
        textView.setText("专题详情页面内容");
    }
}
