package net.mtian.beijingnews.utils;

import android.content.Context;
import android.content.SharedPreferences;

import net.mtian.beijingnews.SplashActivity;

/**
 * Created by jiaciwang on 2016/12/29.
 */

public class CacheUtils {

    /**
     * 得到缓存值
     * @param context 上下文
     * @param key
     * @return
     */
    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences("ataguigu", Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    /**
     * 保存软件的参数
     * @param context
     * @param key
     * @param value
     */
    public static void putBoolean(Context context, String key, boolean value){
        SharedPreferences sp = context.getSharedPreferences("ataguigu", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static void putString(Context content, String key, String value) {
        SharedPreferences sp = content.getSharedPreferences("ataguigu", Context.MODE_PRIVATE);
        sp.edit().putString(key, value).commit();
    }

    public static String getString(Context content, String key) {
        SharedPreferences sp = content.getSharedPreferences("ataguigu", Context.MODE_PRIVATE);
        return sp.getString(key, "");
    }
}
