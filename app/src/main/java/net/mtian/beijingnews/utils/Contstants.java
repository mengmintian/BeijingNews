package net.mtian.beijingnews.utils;

/**
 * Created by mengm on 2017/1/2.
 */

public class Contstants {

    /**
     * 联网请求服务器根地址
     */
    public static final String BASE_URL = "http://192.168.31.233/web_home";
    /**
     * 新闻中心的网络地址
     */
    public static final String NEWSCENTER_PAGER_URL = BASE_URL + "/static/api/news/categories.json";
}
