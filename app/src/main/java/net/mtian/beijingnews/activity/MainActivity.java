package net.mtian.beijingnews.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import net.mtian.beijingnews.R;
import net.mtian.beijingnews.fragment.ContentFragment;
import net.mtian.beijingnews.fragment.LeftMenuFragment;
import net.mtian.beijingnews.utils.DensityUtil;

public class MainActivity extends SlidingFragmentActivity {

    public static final String MAIN_CONTENT_TAG = "main_content_tag";
    public static final String LEFTMENU_TAG = "leftmenu_tag";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        //1、设置SlidingMenu的主页面
        setContentView(R.layout.activity_main);

        //2、设置左侧菜单
        setBehindContentView(R.layout.activity_leftmenu);

        //3、设置右侧菜单
        SlidingMenu slidingMenu = getSlidingMenu();
        slidingMenu.setSecondaryMenu(R.layout.activity_rightmenu);

        //4、设置显示模式：左侧菜单+主页面，左侧菜单+主页面+右侧菜单，主页面+右侧菜单
        slidingMenu.setMode(SlidingMenu.LEFT);

        //5、设置滑动模式：滑动边缘，全屏滑动，不可滑动
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        //6、设置主页占据的宽度
        slidingMenu.setBehindOffset(DensityUtil.dip2px(MainActivity.this, 200));

        //初始化Fragment
        initFragment();

    }

    private void initFragment() {
        //1、得到FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        //2、开启事务
        FragmentTransaction ft = fm.beginTransaction();
        //3、替换
        ft.replace(R.id.fl_main_content, new ContentFragment(), MAIN_CONTENT_TAG); //主页
        ft.replace(R.id.fl_leftmenu, new LeftMenuFragment(), LEFTMENU_TAG); //左侧菜单
        //4、提交
        ft.commit();
    }

    /**
     * 得到左侧菜单
     * @return
     */
    public LeftMenuFragment getLeftMenuFragment() {
//        FragmentManager fm = getSupportFragmentManager();
//        LeftMenuFragment leftMenuFragment = (LeftMenuFragment) fm.findFragmentByTag(LEFTMENU_TAG);
        return (LeftMenuFragment) getSupportFragmentManager().findFragmentByTag(LEFTMENU_TAG);
    }

    /**
     * 得到正文Fragment
     * @return
     */
    public ContentFragment getContextFragment() {
        return (ContentFragment) getSupportFragmentManager().findFragmentByTag(MAIN_CONTENT_TAG);

    }
}
