package net.mtian.beijingnews.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import net.mtian.beijingnews.R;
import net.mtian.beijingnews.SplashActivity;
import net.mtian.beijingnews.utils.CacheUtils;
import net.mtian.beijingnews.utils.DensityUtil;

import java.util.ArrayList;

import static net.mtian.beijingnews.SplashActivity.START_MAIN;

public class GuideActivity extends Activity {

    private ViewPager viewPager;
    private Button btn_start_main;
    private LinearLayout ll_point_group;
    private ImageView iv_point_red;

    private ArrayList<ImageView> imageViews;

    //两点的间距
    private int leftmax;

    //点的大小
    private int widthdpi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        btn_start_main = (Button) findViewById(R.id.btn_start_main);
        ll_point_group = (LinearLayout) findViewById(R.id.ll_point_group);
        iv_point_red = (ImageView) findViewById(R.id.iv_point_red);

        //准备数据
        int[] ids = new int[]{
            R.drawable.guide_1,
            R.drawable.guide_2,
            R.drawable.guide_3,
        };

        widthdpi = DensityUtil.dip2px(this, 10);

        imageViews = new ArrayList<>();
        for (int i = 0; i < ids.length; i++){
            ImageView imageView = new ImageView(this);
            imageView.setBackgroundResource(ids[i]);

            imageViews.add(imageView);

            //创建点
            ImageView point = new ImageView(this);
            point.setBackgroundResource(R.drawable.point_normal);
            //单位是像素
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(widthdpi, widthdpi);
            if (i != 0){
                params.leftMargin = widthdpi;
            }
            point.setLayoutParams(params);
            //添加到线性布局里
            ll_point_group.addView(point);

        }

        //设置ViewPager的适配器
        viewPager.setAdapter(new MyPagerAdapter());

        //根据view的生命周期，当视图执行到onLayout或者onDraw的时候，视图的高和宽、边距都有了
        iv_point_red.getViewTreeObserver().addOnGlobalLayoutListener(new MyOnGlobalLayoutListener());

        btn_start_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //保存记录，已经进入过主页面
                CacheUtils.putBoolean(GuideActivity.this, START_MAIN, true);
                //跳转到主页面
                Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    class MyOnGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener{

        @Override
        public void onGlobalLayout() {
            //只需要执行一次
            iv_point_red.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            leftmax = ll_point_group.getChildAt(1).getLeft() - ll_point_group.getChildAt(0).getLeft();

            //得到屏幕滑动的百分比
            viewPager.addOnPageChangeListener(new MyOnPageChangeListener());
        }
    }

    class MyOnPageChangeListener implements ViewPager.OnPageChangeListener{

        /**
         * 页面滑动时
         * @param position 当前滑动的页面
         * @param positionOffset  百分比
         * @param positionOffsetPixels 滑动的像素
         */
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //红点移动的距离
            int leftmargin = (int) ((positionOffset + position) * leftmax);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) iv_point_red.getLayoutParams();
            params.leftMargin = leftmargin;
            iv_point_red.setLayoutParams(params);
        }

        /**
         * 页面选中时回调
         * @param position
         */
        @Override
        public void onPageSelected(int position) {
            if(position == imageViews.size() - 1){
                btn_start_main.setVisibility(View.VISIBLE);
            }else {
                btn_start_main.setVisibility(View.GONE);
            }
        }

        /**
         * 页面状态改变时回调
         * @param state
         */
        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    class MyPagerAdapter extends PagerAdapter{

        /**
         * 返回数据的总个数
         * @return
         */
        @Override
        public int getCount() {
            return imageViews.size();
        }

        /**
         *
         * @param container ViewPager
         * @param position 要创建页面的位置
         * @return 返回和创建当前页面关系的值
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = imageViews.get(position);
            //添加到容器中
            container.addView(imageView);
            return imageView;
        }

        /**
         * 判断
         * @param view 当前创建的视图
         * @param object instantiateItem返回的结果值
         * @return
         */
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        /**
         *
         * @param container
         * @param position
         * @param object
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }


    }
}
