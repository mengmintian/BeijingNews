package net.mtian.beijingnews.fragment;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import net.mtian.beijingnews.R;
import net.mtian.beijingnews.activity.MainActivity;
import net.mtian.beijingnews.base.BaseFragment;
import net.mtian.beijingnews.domain.NewsCenterPagerBean2;
import net.mtian.beijingnews.pager.NewsCenterPager;
import net.mtian.beijingnews.utils.DensityUtil;
import net.mtian.beijingnews.utils.LogUtil;

import java.util.List;

/**
 * Created by mengm on 2017/1/1.
 */

public class LeftMenuFragment extends BaseFragment{

    private List<NewsCenterPagerBean2.DetailPagerData> data;

    private ListView listView;

    private LeftmenuFragmentAdapter adapter;

    /**
     * 点击item的位置
     */
    private int prePosition;

    @Override
    public View initView() {
        LogUtil.e("左侧菜单视图被初始化了");
        listView = new ListView(context);
        listView.setPadding(0, DensityUtil.dip2px(context, 40), 0, 0);
        listView.setDividerHeight(0);//设置分隔线高度为0
        listView.setCacheColorHint(Color.TRANSPARENT);//设置点击时的颜色为透明
        listView.setSelection(android.R.color.transparent);//设置按下listview的item不变色

        //设置item的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //1、记录点击的位置，变成红色
                prePosition = position;

                adapter.notifyDataSetChanged();//依次执行adapter的方法getCount()->getView()
                //2、把左侧菜单关闭
                MainActivity mainActivity = (MainActivity) context;
                mainActivity.getSlidingMenu().toggle();

                //3、切换到对应的详情页面
                switchPager(prePosition);
            }
        });
        return listView;
    }

    /**
     * 根据位置切换不同的详情页面
     * @param position
     */
    public void switchPager(int position){
        MainActivity mainActivity = (MainActivity) context;
        ContentFragment contextFragment = mainActivity.getContextFragment();
        NewsCenterPager newsCenterPager = contextFragment.getNewsCenterPager();
        newsCenterPager.switchPager(position);
    }

    @Override
    public void initData() {
        super.initData();
        LogUtil.e("左侧菜单数据被初始化了");
    }

    /**
     * 接收数据
     * @param data
     */
    public void setData(List<NewsCenterPagerBean2.DetailPagerData> data) {
        this.data = data;
        for (int i=0; i<data.size(); i++){
            LogUtil.e("title===="+data.get(i).getTitle());
        }

        //设置适配器
        adapter = new LeftmenuFragmentAdapter();
        listView.setAdapter(adapter);

        //设置新闻详情默认页面
        switchPager(prePosition);
    }

    class LeftmenuFragmentAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = (TextView) View.inflate(context, R.layout.item_leftmenu, null);
            textView.setText(data.get(position).getTitle());

            //标记当前item为红色
            textView.setEnabled(prePosition == position);

            return textView;
        }
    }
}
