package net.mtian.beijingnews.fragment;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import net.mtian.beijingnews.R;
import net.mtian.beijingnews.activity.MainActivity;
import net.mtian.beijingnews.adapter.ContentFragmentAdapter;
import net.mtian.beijingnews.base.BaseFragment;
import net.mtian.beijingnews.base.BasePager;
import net.mtian.beijingnews.pager.GovaffairPager;
import net.mtian.beijingnews.pager.HomePager;
import net.mtian.beijingnews.pager.NewsCenterPager;
import net.mtian.beijingnews.pager.SettingPager;
import net.mtian.beijingnews.pager.SmartServicePager;
import net.mtian.beijingnews.utils.LogUtil;
import net.mtian.beijingnews.view.NoScrollViewPager;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;

/**
 * Created by mengm on 2017/1/1.
 */

public class ContentFragment extends BaseFragment{

    @ViewInject(R.id.viewPager)
    private NoScrollViewPager  viewPager;

    @ViewInject(R.id.rg_main)
    private RadioGroup rg_main;

    /**
     * 装一个页面的集合
     */
    private ArrayList<BasePager> basePagers;

    @Override
    public View initView() {
        LogUtil.e("正文视图被初始化了");
        View view =  View.inflate(context, R.layout.content_fragment, null);

//        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
//        rg_main = (RadioGroup) view.findViewById(R.id.rg_main);

        //1、把视图注入到框架中，让ContentFragment和视图关联起来
        x.view().inject(ContentFragment.this, view);
        return view;
    }

    @Override
    public void initData() {
        super.initData();
        LogUtil.e("正文数据被初始化了");

        //初始化一个页面，并且放入集合中
        basePagers = new ArrayList<>();
        basePagers.add(new HomePager(context));
        basePagers.add(new NewsCenterPager(context));
        basePagers.add(new SmartServicePager(context));
        basePagers.add(new GovaffairPager(context));
        basePagers.add(new SettingPager(context));



        //设置viewPager的适配器
        viewPager.setAdapter(new ContentFragmentAdapter(basePagers));

        //设置RadioGroup的选中状态改变的监听
        rg_main.setOnCheckedChangeListener(new MyOnCheckedChangeListener());

        //监听某个页面被选中，初始化对应页面的数据
        viewPager.addOnPageChangeListener(new MyOnPageChangeListener());

        //设置默认选中首页
        rg_main.check(R.id.rb_home);
        basePagers.get(0).initData();
        //默认不可滑动左菜单
        isEnableSlidingMenu(SlidingMenu.TOUCHMODE_NONE);
    }

    /**
     * 根据传入的参数设置是否让SlidingMenu可以滑动
     * @param touchmodefullscreen
     */
    private void isEnableSlidingMenu(int touchmodefullscreen) {
        MainActivity mainActivity = (MainActivity) context;
        mainActivity.getSlidingMenu().setTouchModeAbove(touchmodefullscreen);
    }

    /**
     * 得到新闻中心
     * @return
     */
    public NewsCenterPager getNewsCenterPager() {
        return (NewsCenterPager) basePagers.get(1);
    }

    class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        /**
         * 当某个页面被选中的时候回调
         * @param position
         */
        @Override
        public void onPageSelected(int position) {
            //调用被选中的页面的initData方法
            basePagers.get(position).initData();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }



    class MyOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener{

        /**
         *
         * @param group RadioGroup
         * @param checkedId 被选中的RadioButton的id
         */
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId){
                case R.id.rb_home:
                    viewPager.setCurrentItem(0, false);//加上false表示切换没有动画
                    isEnableSlidingMenu(SlidingMenu.TOUCHMODE_NONE);
                    break;
                case R.id.rb_newscenter:
                    viewPager.setCurrentItem(1, false);
                    isEnableSlidingMenu(SlidingMenu.TOUCHMODE_FULLSCREEN);
                    break;
                case R.id.rb_smartservice:
                    viewPager.setCurrentItem(2, false);
                    isEnableSlidingMenu(SlidingMenu.TOUCHMODE_NONE);
                    break;
                case R.id.rb_govaffair:
                    viewPager.setCurrentItem(3, false);
                    isEnableSlidingMenu(SlidingMenu.TOUCHMODE_NONE);
                    break;
                case R.id.rb_setting:
                    viewPager.setCurrentItem(4, false);
                    isEnableSlidingMenu(SlidingMenu.TOUCHMODE_NONE);
                    break;
            }
        }

    }

}
