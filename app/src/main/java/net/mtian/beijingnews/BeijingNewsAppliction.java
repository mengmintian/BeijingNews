package net.mtian.beijingnews;

import android.app.Application;

import org.xutils.x;

/**
 * Created by mengm on 2017/1/1.
 */

public class BeijingNewsAppliction extends Application {

    /**
     * 所有组件被创建之前执行，一定要在功能清单里配置
     */
    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.setDebug(true);
        x.Ext.init(this);
    }
}
